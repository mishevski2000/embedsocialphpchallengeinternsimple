<?php

require_once "./Review.php";

// GETS DATA, FILTER FOR MINIMUM RATING OF REVIEW AND WHAT FILTER WAS USED WITH HELP OF AJAX CALL

$data = $_POST['data'];
$minimumRating = $_POST['minimumRating'];
$filter = $_POST['filter'];

// IF STATEMNTS TO CHECK WHICH OF THE FILTER IS BEGIN USED, THEN SORTING AND PRINTING THE DATA INSIDE THE TABLE

if($filter === "orderByRatingHigh")
{

    //IF THE SORT_BY_RATING_HIGH IS USED IT WILL SORT THE DATA FROM HIGHEST TO LOWEST AND PRINT ONLY THE DATA GREATER OF EQUEL TO THE MINIMAL RATING FILTER

    usort($data, "SortByRatingHigh");

    foreach($data as $review){
        if($review['rating'] >= $minimumRating){

        PrintReview($review);

        }
    }
}
elseif($filter === "orderByRatingLow")
{

    //IF THE SORT_BY_RATING_LOW IS USED IT WILL SORT THE DATA FROM LOWEST TO HIGHEST AND PRINT ONLY THE DATA GREATER OF EQUEL TO THE MINIMAL RATING FILTER

    usort($data, "SortByRatingLow");

    foreach($data as $review){
        if($review['rating'] >= $minimumRating){

        PrintReview($review);

        }
    }
}
elseif($filter === "orderByDateDesc")
{

    //IF THE SORT_BY_DATE_DESC IS USED IT WILL SORT THE DATA FROM NEWEST TO OLDEST AND PRINT ONLY THE DATA GREATER OF EQUEL TO THE MINIMAL RATING FILTER

    usort($data, "SortbyDateDesc");

    foreach($data as $review){
        if($review['rating'] >= $minimumRating){

        PrintReview($review);

        }
    }
}
elseif($filter === "orderByDateAsc")
{

    //IF THE SORT_BY_DATE_ASC IS USED IT WILL SORT THE DATA FROM OLDEST TO NEWEST AND PRINT ONLY THE DATA GREATER OF EQUEL TO THE MINIMAL RATING FILTER

    usort($data, "SortbyDateAsc");

    foreach($data as $review){
        if($review['rating'] >= $minimumRating){

        PrintReview($review);

        }
    }
}
elseif($filter === "prioritizeByTextYes")
{

    //IF THE PRIORITIZE_BY_TEXT_YES IT WILL SORT THE DATA FROM REVIEWS WITH TEXT TO WIHTOUT AND PRINT ONLY THE DATA GREATER OF EQUEL TO THE MINIMAL RATING FILTER

    usort($data, "PrioritizeByText");

    foreach($data as $review){
        if($review['rating'] >= $minimumRating){

        PrintReview($review);

        }
    }
}
elseif($filter === "prioritizeByTextNo")
{

    //IF THE PRIORITIZE_BY_TEXT_NONE IT WILL ONLY PRINT THE DATA GREATE OR EQUEL TO THE MININAL RATING FILTER WIHOUT SORTING

    foreach($data as $review){
        if($review['rating'] >= $minimumRating){

        PrintReview($review);

        }
    }
}
?>

