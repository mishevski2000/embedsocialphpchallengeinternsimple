$( document ).ready(function() {

    // AJAX CALL TO PRINT TO PRINT REVIEWS

    fetch("./reviews.json")
        .then(function(resp) {
            return resp.json();
        })
        .then(function(data) {
            $.ajax({
                url:"./showData.php",
                method: "POST",
                data:{
                    data: data,
                    minimunRating: 1 
                },
                success:function(data){
                    $("tbody").html(data);
                }

            })

            
        });

        // EVENT LISTENER TO STORE MINIMUM RATING FILTER

        let minimumRating = 1;

        $("select#minimum-rating").change(function(){
            minimumRating  = $(this).children("option:selected").val();
        });

        // FUNCTION TO MAKE AUTOMATIC AJAX CALLS FOR EACH FILTER

        let AjaxCall = function(rating, filter){
            fetch("./reviews.json")
            .then(function(resp) {
                return resp.json();
            })
            .then(function(data) {
                $.ajax({
                    url:"./showFiltered.php",
                    method: "POST",
                    data:{
                        data: data,
                        minimumRating: rating,
                        filter: filter 
                    },
                    success:function(data){
                        $("tbody").html(data);
                    }
        
                })
            });
        }

    
    // EVENT LISTENER TO CHANGE BUTTON FOR RATING FILTER AND MAKE A AJAX CALL TO FILTER BY RATING HIGHEST FIRST    
    $("#order-by-rating-high").click(function(e)
    {
        e.preventDefault();
        $("#order-by-rating-high").css("display", "none");
        $("#order-by-rating-low").css("display", "block");

        
        AjaxCall(minimumRating, "orderByRatingHigh");
        
    })

    // EVENT LISTENER TO CHANGE BUTTON FOR RATING FILTER AND MAKE A AJAX CALL TO FILTER BY RATING LOWEST FIRST

    $("#order-by-rating-low").click(function(e)
    {
        e.preventDefault();
        $("#order-by-rating-low").css("display", "none");
        $("#order-by-rating-high").css("display", "block");

        AjaxCall(minimumRating, "orderByRatingLow");
    })

    // EVENT LISTENER TO CHANGE BUTTON FOR DATE FILTER AND MAKE A AJAX CALL TO FILTER BY DATE NEWEST ONE
    
    $("#order-by-date-desc").click(function(e)
    {
        e.preventDefault();
        $("#order-by-date-desc").css("display", "none");
        $("#order-by-date-asc").css("display", "block");

        AjaxCall(minimumRating, "orderByDateDesc");
    })

    // EVENT LISTENER TO CHANGE BUTTON FOR DATE FILTER AND MAKE A AJAX CALL TO FILTER BY DATE OLDEST ONE

    $("#order-by-date-asc").click(function(e)
    {
        e.preventDefault();
        $("#order-by-date-asc").css("display", "none");
        $("#order-by-date-desc").css("display", "block");

        AjaxCall(minimumRating, "orderByDateAsc");
    })

    // EVENT LISTENER TO CHANGE BUTTON FOR TEXT PRIORITY FILTER AND MAKE A AJAX CALL TO FILTER BY TEXT PRIORTY YES

    $("#prioritize-by-text-yes").click(function(e)
    {
        e.preventDefault();
        $("#prioritize-by-text-yes").css("display", "none");
        $("#prioritize-by-text-no").css("display", "block");

        AjaxCall(minimumRating, "prioritizeByTextYes");
    })

    // EVENT LISTENER TO CHANGE BUTTON FOR TEXT PRIORITY FILTER AND MAKE A AJAX CALL TO SHOW DEFAULT REVIEWS WIHOUT TEXT PRIORITY

    $("#prioritize-by-text-no").click(function(e)
    {
        e.preventDefault();
        $("#prioritize-by-text-no").css("display", "none");
        $("#prioritize-by-text-yes").css("display", "block");

        AjaxCall(minimumRating, "prioritizeByTextNo");
    })
});