<?php

// FUNCTION TO PRINT DATA INSIDE OD TABLE

function PrintReview($review)
{
    echo "
    <tr>
        <td>{$review['reviewerName']}</td>
        <td>{$review['rating']}</td>
        <td>{$review['reviewCreatedOnDate']}</td>
        <td>{$review['reviewFullText']}</td>
    </tr> 
";
}

// FUNCTION FOR SORTING THE DATA FROM HIGHEST RATING TO LOWEST

function SortByRatingHigh($a, $b)
{
return strcmp($b['rating'], $a['rating']);
}

// FUNCTION FOR SORTING THE DATA FROM LOWEST RATING TO HIGHEST

function SortByRatingLow($a, $b)
{
return strcmp($a['rating'], $b['rating']);
}

// FUNCTION FOR SORTING THE DATA FROM MOST RECENT TO OLDEST

function SortbyDateDesc($a, $b){
    return strcmp($b['reviewCreatedOnDate'], $a['reviewCreatedOnDate']);
}

// FUNCTION FOR SORTING THE DATA FROM OLDEST TO MOST RECENT

function SortbyDateAsc($a, $b){
    return strcmp($a['reviewCreatedOnDate'], $b['reviewCreatedOnDate']);
}

// FUNCTION FOR PRIORITIZE THE REVIEWS BY TEXT

function PrioritizeByText($a){
    if($a['reviewText'] != ""){
        return -1;
    }else{
        return 1;
    }
}

?>